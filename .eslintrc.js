const OFF = 0;
const WARNING = 1;
const ERROR = 2;

module.exports = {
	env: {
		browser: true,
		es6: true,
		jquery: true
	},
	extends: [
		'standard'
	],
	globals: {
		Atomics: 'readonly',
		SharedArrayBuffer: 'readonly'
	},
	parser: 'babel-eslint',
	parserOptions: {
		ecmaVersion: 2018,
		sourceType: 'module'
	},
	rules: {
		'no-tabs': OFF,
		'indent': [WARNING, 'tab', {
			SwitchCase: 1,
			MemberExpression: OFF
		}],
		'space-before-function-paren': OFF,
		'semi': [WARNING, 'always'],
		'padded-blocks': OFF,
		// require `function foo()` instead of `function foo ()`
		'space-before-function-paren': [
			WARNING,
			{ anonymous: 'never', named: 'never' },
		],
		// Best practices
		// Prefer destructuring from arrays and objects
		// https://eslint.org/docs/rules/prefer-destructuring
		'prefer-destructuring': [WARNING,
			{
				VariableDeclarator: {
					array: false,
					object: true,
				},
				AssignmentExpression: {
					array: true,
					object: false,
				},
			}, {
				enforceForRenamedProperties: false,
		}],
		// suggest using of const declaration for variables that are never modified after declared
		'prefer-const': [ERROR, {
			destructuring: 'any',
			ignoreReadBeforeAssign: true,
		}],
		// use rest parameters instead of arguments
    	// https://eslint.org/docs/rules/prefer-rest-params
		'prefer-rest-params': ERROR,
		// import sorting
		// https://eslint.org/docs/rules/sort-imports
		'sort-imports': ['off', {
			ignoreCase: false,
			ignoreDeclarationSort: false,
			ignoreMemberSort: false,
			memberSyntaxSortOrder: ['none', 'all', 'multiple', 'single'],
		}],
		'no-console': [WARNING, { allow: ['warn', 'error'] }]
	}
}
