# @steficon/browser-style
This is the eslint configuration as a package that we use in steficon for vanilla and jquery javascript projects. It is based on standard.js coding style altered to our style needs and best practices

## Prerequisites
- You will need to have eslint installed in your project or globally and a package.json file already setup in your project
- You need to be authorized as a bitbucket steficon user

## Installation
Add the package inside your dependencies list in package.json
```
"dependencies": {
	"@steficon/browser-style": "git+https://bitbucket.org/steficon/javascript-browser-style.git"
}
```
And run `npm install` or `npm install @steficon/browser-style`

(this might take a little while on the 1st run since it will take care of all the dependencies for you)

## Setup
Create a .eslintrc.js file and import the package
```
touch .eslintrc.js

```
Import the package in `.eslintrc.js`
```
module.exports = require('@steficon/browser-style');
```

That's it.

